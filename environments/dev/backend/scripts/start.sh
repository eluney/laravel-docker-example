#!/usr/bin/env bash

set -e

# Run our defined exec if args empty
if [ -z "$1" ]; then

    role=${CONTAINER_ROLE:-sitio}

    env=${LARAVEL_APP_ENV:-production}

    cd /var/www;

    [ ! -d "vendor" ] && \
        (composer install && php artisan migrate && php artisan db:seed) && \
        php artisan key:generate

    [ "$env" != "local" ] && (php artisan cache:optimize)

    [ "$role" = "backend" ] && exec apache2-foreground;

    [ "$role" = "queue" ] && exec php artisan queue:work --verbose --tries=3 --timeout=90;

    if [ "$role" = "scheduler" ]; then

        while [ true ]; do

            exec php artisan schedule:run --verbose --no-interaction & sleep 60

        done

    else

        echo "Could not match the container role \"$role\""

        exit 1

    fi

else

    exec "$@"

fi
