# Ejemplo de entorno en docker para Laravel

## Requerimientos

Se requiere la instalación de [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git), [docker](https://docs.docker.com/engine/install/) y [docker-compose](https://docs.docker.com/compose/install/).

## Instalación

 - Clonar el proyecto
 - Ejecutar el script "init.sh" y seleccionar el ambiente para desplegar
 - Reconstruir la imagen

```bash
docker-compose up -d
```

## Uso

# Desarrollo con docker

### Refrescar la base y correr los seeders

```bash
docker compose exec backend php artisan migrate:fresh --seed
```

### Instalar dependencias de composer

```bash
docker-compose exec backend composer install
```

