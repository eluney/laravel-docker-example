#!/bin/bash

function copy_files()
{
    files=("$@")
    for i in "${files[@]}"
    do
        echo "Copiado: $i"
        cp $i
    done
}

function if_not_exists_create_folder()
{
    folders=("$@")
    for i in "${folders[@]}"
    do
        if [ ! -d $i ]; then
            echo "Creando carpeta: $i"
            mkdir $i;
        fi
    done
}

function set_environment()
{
    [ "$1" = "test" ] && echo 'Configurando laravel como entorno test' \
        && sed -i 's/local/test/' .env \
        && sed -i 's/LARAVEL_APP_DEBUG=true/LARAVEL_APP_DEBUG=false/' .env;

    [ "$1" = "production" ] && echo 'Configurando laravel como entorno production' \
        && sed -i 's/local/production/' .env \
        && sed -i 's/LARAVEL_APP_DEBUG=true/LARAVEL_APP_DEBUG=false/' .env;

    [ "$1" = "desarrollo" ] && echo 'Configurando laravel como entorno local' \
        && sed -i 's/production/local/' .env \
        && sed -i 's/LARAVEL_APP_DEBUG=false/LARAVEL_APP_DEBUG=true/' .env;
}

files_production=(
    '.env.example .env' 
    'src/backend/.env.example src/backend/.env'
    'docker-compose.yml.production docker-compose.yml'
    'nginx/default.conf.template.production nginx/default.conf.template'
    )

files_test=(
    './environments/test/.env.example .env' 
    './src/backend/.env.example src/backend/.env'
    './environments/test/nginx/conf.template nginx/default.conf.template'
    './environments/test/docker-compose.yml.example docker-compose.yml'
    )

files_develop=(
    './environments/dev/.env.example ./.env' 
    './src/backend/.env.example ./src/backend/.env'
    './environments/dev/nginx/conf.template ./nginx/default.conf.template'
    './environments/dev/docker-compose.yml.example ./docker-compose.yml'
    )

folders=('./nginx')

mainmenu() {
    echo -ne "
MENÚ
1) Copiar archivos de configuración y setear entorno de desarrollo
2) Copiar archivos de configuración y setear entorno de test
3) Copiar archivos de configuración y setear entorno de producción
0) Salir
Elige una opción:  "
    read -r ans
    case $ans in
    1)
        if_not_exists_create_folder "${folders[@]}";
        (copy_files "${files_develop[@]}" && set_environment 'desarrollo' ) && \
        echo '--------------------';
        echo 'Construyendo imagen de backend';
        docker compose build backend;
        echo '--------------------';
        echo 'Para levantar el proyecto utilice el comando "docker compose up -d"';
        echo 'El primer inicio puede demorar un poco debido a la instalación de dependencias';
        ;;
    2)
        if_not_exists_create_folder "${folders[@]}";
        #(copy_files "${files_test[@]}" && set_environment 'test' )
        echo 'No implentado aun';
        ;;
    3)
        #(copy_files "${files_production[@]}" && set_environment 'production' )
        echo 'No implentado aun';
        ;;
    0)
        echo "Adios.";
        exit 0;
        ;;
    *)
        echo "¡Opción incorrecta!";
        exit 1;
        ;;
    esac
}

mainmenu
